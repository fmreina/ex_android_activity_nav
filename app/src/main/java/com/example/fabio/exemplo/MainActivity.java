package com.example.fabio.exemplo;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void onNoticiasClick(View view){
        Intent intent = new Intent(this, NoticiasActivity.class);
        startActivity(intent);
    }

    public void onEmpreendimentosClick(View view){
        Intent intent = new Intent(this, EmpreendimentosActivity.class);
        startActivity(intent);
    }

    public void onVideosClick(View view){
        Intent intent = new Intent(this, VideoActivity.class);
        startActivity(intent);
    }
}
